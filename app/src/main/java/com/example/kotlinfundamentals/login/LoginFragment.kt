package com.example.kotlinfundamentals.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.kotlinfundamentals.database.UserDatabase
import com.example.kotlinfundamentals.R
import com.example.kotlinfundamentals.databinding.LoginFragmentBinding
import com.google.android.material.snackbar.Snackbar

class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding : LoginFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
         binding = DataBindingUtil.inflate<LoginFragmentBinding>(inflater,
            R.layout.login_fragment,container,false)

        //(activity as AppCompatActivity).supportActionBar?.hide()

        val application = requireNotNull(this.activity).application

        val dataSource = UserDatabase.getInstance(application).userDao
        val viewModelFactory = LoginViewModelFactory(dataSource, application)

        viewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(LoginViewModel::class.java)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

//        binding.emailVariable = binding.txtLoginEmail.text.toString()
//        binding.passwordVariable = binding.txtLoginPassword.text.toString()

        observeUI()

        return binding.root
    }

    private fun observeUI() {
        viewModel.emailFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.passwordFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.navigateToSignUp.observe(viewLifecycleOwner, Observer { navigateTo ->
            if (navigateTo) {
                onSignUpPressed()
            }
        })

        viewModel.loginButtonClicked.observe(viewLifecycleOwner, Observer { loginUser ->
            if (loginUser) {
                onDoLogIn()
            }
        })
    }

    private fun onSignUpPressed() {
        findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        viewModel.doneNavigatingToSignUp()
    }

    private fun onDoLogIn() {
        var snack = Snackbar.make(requireActivity().findViewById(android.R.id.content),
            viewModel.loginMessage.value.toString() + " " + viewModel.email.value + " " + viewModel.password.value,
            Snackbar.LENGTH_SHORT)
        snack.show()

        if (viewModel.loginStatus.value!!) {

            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
        }

        viewModel.doneLoginButtonClicked()
    }
}