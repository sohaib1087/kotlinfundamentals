package com.example.kotlinfundamentals.login

import android.app.Application
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.kotlinfundamentals.database.UserDao
import kotlinx.coroutines.*

class LoginViewModel(private val userDao: UserDao, application: Application) : AndroidViewModel(application), Observable {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    @Bindable
    val emailFieldContent = MutableLiveData<String>()

    @Bindable
    val passwordFieldContent = MutableLiveData<String>()

    private val _email = MutableLiveData<String>()
    val email: LiveData<String>
    get() = _email

    private val _password = MutableLiveData<String>()
    val password: LiveData<String>
        get() = _password

    private val _navigateToSignUp = MutableLiveData<Boolean>()
    val navigateToSignUp: LiveData<Boolean>
        get() = _navigateToSignUp

    private val _loginButtonClicked = MutableLiveData<Boolean>()
    val loginButtonClicked: LiveData<Boolean>
        get() = _loginButtonClicked

    private val _loginMessage = MutableLiveData<String>()
    val loginMessage: LiveData<String>
        get() = _loginMessage

    private val _loginStatus = MutableLiveData<Boolean>()
    val loginStatus: LiveData<Boolean>
        get() = _loginStatus

    init {
        emailFieldContent.value = ""
        passwordFieldContent.value = ""
        _email.value = ""
        _password.value = ""
        _navigateToSignUp.value = false
        _loginButtonClicked.value = false
        _loginMessage.value = ""
        _loginStatus.value = false
    }

    fun navigateToSignUp() {
        _navigateToSignUp.value = true
    }

    fun doneNavigatingToSignUp() {
        _navigateToSignUp.value = false
    }

    fun loginButtonClicked() {
        _email.value = emailFieldContent.value
        _password.value = passwordFieldContent.value
        uiScope.launch {
            val result = authorizeUser()
            if (result > 0) {
                _loginMessage.value = "Successfully Logged In!"
                _loginStatus.value = true
            }
            else {
                _loginMessage.value = "Invalid Credentials!"
                _loginStatus.value = false
            }
            _loginButtonClicked.value = true
        }
    }

    fun doneLoginButtonClicked() {

        _loginButtonClicked.value = false
        _loginMessage.value = ""
        _loginStatus.value = false
    }

    private suspend fun authorizeUser() : Long {

        return withContext(Dispatchers.IO) {
            var userId = userDao.authorizeUser(_email.value.toString(), _password.value.toString())
            userId
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }
}