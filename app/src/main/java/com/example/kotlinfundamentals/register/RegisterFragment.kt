package com.example.kotlinfundamentals.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.kotlinfundamentals.database.UserDatabase
import com.example.kotlinfundamentals.R
import com.example.kotlinfundamentals.databinding.RegisterFragmentBinding
import com.google.android.material.snackbar.Snackbar

class RegisterFragment : Fragment() {

    private lateinit var viewModel: RegisterViewModel
    private lateinit var binding : RegisterFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
         binding = DataBindingUtil.inflate<RegisterFragmentBinding>(inflater,
            R.layout.register_fragment,container,false)

        val application = requireNotNull(this.activity).application

        val dataSource = UserDatabase.getInstance(application).userDao
        val viewModelFactory = RegisterViewModelFactory(dataSource, application)

        viewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(RegisterViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        observeUI()

        return binding.root
    }

    private fun observeUI() {
        viewModel.emailFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.nameFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.passwordFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.repeatPasswordFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.navigateToLogIn.observe(viewLifecycleOwner, Observer { navigateTo ->
            if (navigateTo) {
                onLogInPressed()
            }
        })

        viewModel.signUpButtonClicked.observe(viewLifecycleOwner, Observer { signUpUser ->
            if (signUpUser) {
                onDoSignUp()
            }
        })
    }

    private fun onLogInPressed() {
        findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment())
        viewModel.doneNavigatingToLogIn()
    }

    private fun onDoSignUp() {
        var snack = Snackbar.make(requireActivity().findViewById(android.R.id.content),
            viewModel.signUpMessage.value.toString() + " " + viewModel.email.value,
            Snackbar.LENGTH_SHORT)
        snack.show()
        onLogInPressed()
        viewModel.doneSignUpButtonClicked()
    }
}