package com.example.kotlinfundamentals.register

import android.app.Application
import androidx.databinding.Bindable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.kotlinfundamentals.database.User
import com.example.kotlinfundamentals.database.UserDao
import kotlinx.coroutines.*
import androidx.databinding.Observable

class RegisterViewModel (private val userDao: UserDao, application: Application) : AndroidViewModel(application), Observable {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    @Bindable
    val emailFieldContent = MutableLiveData<String>()

    @Bindable
    val passwordFieldContent = MutableLiveData<String>()

    @Bindable
    val nameFieldContent = MutableLiveData<String>()

    @Bindable
    val repeatPasswordFieldContent = MutableLiveData<String>()

    private val _name = MutableLiveData<String>()
    val name: LiveData<String>
        get() = _name

    private val _email = MutableLiveData<String>()
    val email: LiveData<String>
        get() = _email

    private val _password = MutableLiveData<String>()
    val password: LiveData<String>
        get() = _password

    private val _repeatPassword = MutableLiveData<String>()
    val repeatPassword: LiveData<String>
        get() = _repeatPassword

    private val _navigateToLogIn = MutableLiveData<Boolean>()
    val navigateToLogIn: LiveData<Boolean>
        get() = _navigateToLogIn

    private val _signUpButtonClicked = MutableLiveData<Boolean>()
    val signUpButtonClicked: LiveData<Boolean>
        get() = _signUpButtonClicked

    private val _signUpMessage = MutableLiveData<String>()
    val signUpMessage: LiveData<String>
        get() = _signUpMessage

    init {
        emailFieldContent.value = ""
        passwordFieldContent.value = ""
        nameFieldContent.value = ""
        repeatPasswordFieldContent.value = ""
        _name.value = ""
        _email.value = ""
        _password.value = ""
        _repeatPassword.value = ""
        _navigateToLogIn.value = false
        _signUpButtonClicked.value = false
        _signUpMessage.value = ""
    }

    fun navigateToLogIn() {
        _navigateToLogIn.value = true
    }

    fun doneNavigatingToLogIn() {
        _navigateToLogIn.value = false
    }

    fun signUpButtonClicked() {
        _email.value = emailFieldContent.value
        _password.value = passwordFieldContent.value
        _name.value = nameFieldContent.value
        _repeatPassword.value = passwordFieldContent.value
        uiScope.launch {
            val user = User()
            user.email = _email.value.toString()
            user.name = _name.value.toString()
            user.password = _password.value.toString()
            _signUpMessage.value = registerUser(user)
            _signUpButtonClicked.value = true
        }
    }

    fun doneSignUpButtonClicked() {
        _signUpButtonClicked.value = false
        _signUpMessage.value = ""
    }

    private suspend fun registerUser(user: User) : String {
        return withContext(Dispatchers.IO) {
            try {
                //userDao.clearTable()
                userDao.insert(user)
                "Sign Up Successful! Please Log In to continue."
            }
            catch (e: Exception) {
                e.message.toString()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }
}