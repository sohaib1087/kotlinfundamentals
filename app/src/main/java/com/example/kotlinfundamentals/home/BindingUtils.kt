package com.example.kotlinfundamentals.home

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.kotlinfundamentals.database.User

@BindingAdapter("")
fun TextView.setUserName(item: User) {

    text = item.name
}