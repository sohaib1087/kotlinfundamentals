package com.example.kotlinfundamentals.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.kotlinfundamentals.database.UserDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

class HomeViewModel (private val userDao: UserDao, application: Application) : AndroidViewModel(application) {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _navigateToUserDetail = MutableLiveData<Long>()
    val navigateToUserDetail
        get() = _navigateToUserDetail

    val users = userDao.getAllUsers()

    fun onUserClicked(id: Long) {
        _navigateToUserDetail.value = id
    }

    fun onUserNavigated() {
        _navigateToUserDetail.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}