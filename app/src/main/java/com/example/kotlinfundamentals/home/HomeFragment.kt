package com.example.kotlinfundamentals.home

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.kotlinfundamentals.database.UserDatabase
import com.example.kotlinfundamentals.R
import com.example.kotlinfundamentals.databinding.HomeFragmentBinding

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding : HomeFragmentBinding
    private lateinit var drawerLayout: DrawerLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<HomeFragmentBinding>(inflater,
            R.layout.home_fragment,container,false)

        //(activity as AppCompatActivity).supportActionBar?.show()
        (activity as AppCompatActivity).supportActionBar?.title = "All Users"

        val application = requireNotNull(this.activity).application

        val dataSource = UserDatabase.getInstance(application).userDao
        val viewModelFactory = HomeViewModelFactory(dataSource, application)

        viewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(HomeViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val adapter = HomeAdapter(UserClickListener { userId ->

            Toast.makeText(context, "$userId", Toast.LENGTH_SHORT).show()
            viewModel.onUserClicked(userId)
        })

        binding.usersList.adapter = adapter

        viewModel.users.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        viewModel.navigateToUserDetail.observe(viewLifecycleOwner, Observer { user ->
            user?.let {
                this.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToUserDetailFragment(user))
                viewModel.onUserNavigated()
            }

        })

        setHasOptionsMenu(true)
//        drawerLayout = (activity as AppCompatActivity).findViewById(R.id.drawer_layout)
//        NavigationUI.setupActionBarWithNavController((activity as AppCompatActivity), findNavController(), drawerLayout)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return NavigationUI.onNavDestinationSelected(item!!,
//            view!!.findNavController())
//                || super.onOptionsItemSelected(item)
//    }

}
