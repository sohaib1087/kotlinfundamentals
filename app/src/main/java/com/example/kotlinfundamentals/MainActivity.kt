package com.example.kotlinfundamentals

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

        //navController = getNavController()
        //val navHostFragment = supportFragmentManager.findFragmentById(R.id.myNavHostFragment) as NavHostFragment
        //val navController = navHostFragment.navController
        //navController.setGraph(R.navigation.navigation)

        val navController = this.findNavController(R.id.myNavHostFragment)

        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.navView)

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        NavigationUI.setupWithNavController(navView, navController)

        requestedOrientation =  (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.myNavHostFragment)
        return NavigationUI.navigateUp(navController, drawerLayout)
    }

//    private fun getNavController(): NavController {
//        val fragment: Fragment? =
//            supportFragmentManager.findFragmentById(R.id.myNavHostFragment)
//        check(fragment is NavHostFragment) {
//            ("Activity " + this
//                    + " does not have a NavHostFragment")
//        }
//        return (fragment as NavHostFragment?)!!.navController
//    }
}
