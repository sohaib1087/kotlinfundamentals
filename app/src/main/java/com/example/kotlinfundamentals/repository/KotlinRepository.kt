package com.example.kotlinfundamentals.repository

import com.example.kotlinfundamentals.database.User
import com.example.kotlinfundamentals.database.UserDao

class KotlinRepository (private val userDao: UserDao) {

    suspend fun insertUser(user: User) {
        userDao.insert(user)
    }
}