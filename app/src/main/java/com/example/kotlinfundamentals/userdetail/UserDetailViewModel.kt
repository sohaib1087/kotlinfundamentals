package com.example.kotlinfundamentals.userdetail

import android.app.Application
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.kotlinfundamentals.database.User
import com.example.kotlinfundamentals.database.UserDao
import com.example.kotlinfundamentals.network.MyApi
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserDetailViewModel (private val userDao: UserDao, application: Application) : AndroidViewModel(application), Observable {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    @Bindable
    val emailFieldContent = MutableLiveData<String>()

    @Bindable
    val passwordFieldContent = MutableLiveData<String>()

    @Bindable
    val nameFieldContent = MutableLiveData<String>()

    private val _userId = MutableLiveData<Long>()
    val userId: LiveData<Long>
        get() = _userId

    private val _name = MutableLiveData<String>()
    val name: LiveData<String>
        get() = _name

    private val _email = MutableLiveData<String>()
    val email: LiveData<String>
        get() = _email

    private val _password = MutableLiveData<String>()
    val password: LiveData<String>
        get() = _password

    private val _deleteButtonClicked = MutableLiveData<Boolean>()
    val deleteButtonClicked: LiveData<Boolean>
        get() = _deleteButtonClicked

    private val _updateButtonClicked = MutableLiveData<Boolean>()
    val updateButtonClicked: LiveData<Boolean>
        get() = _updateButtonClicked

    private val _imageUrl = MutableLiveData<String>()
    val imageUrl: LiveData<String>
        get() = _imageUrl

    init {
        _name.value = ""
        _email.value = ""
        _password.value = ""
        _userId.value = 0L
        emailFieldContent.value = ""
        passwordFieldContent.value = ""
        nameFieldContent.value = ""
        _deleteButtonClicked.value = false
        _updateButtonClicked.value = false
        _imageUrl.value = ""
    }

    fun onUserClicked(id: Long) {
        _userId.value = id
        //getImageFromApi()
    }

    fun onUserClickedDone() {
        uiScope.launch {
            val user = getUserDetailsById()
            nameFieldContent.value = user?.name
            emailFieldContent.value = user?.email
            passwordFieldContent.value = user?.password
        }
        //_userId.value = null
    }

    private suspend fun getUserDetailsById() : User {
        return withContext(Dispatchers.IO) {
            val user = _userId.value?.let { userDao.getUserById(it) }
            user!!
        }
    }

    private fun getUserDataFromUi() : User {
        val user = User()
        user.userId = _userId.value!!
        user.name = nameFieldContent.value.toString()
        user.email = emailFieldContent.value.toString()
        user.password = passwordFieldContent.value.toString()

        return user
    }

    private suspend fun updateUser() {
        withContext(Dispatchers.IO) {
            val user = getUserDataFromUi()
            userDao.update(user)
        }
    }

    private suspend fun deleteUser() {
        withContext(Dispatchers.IO) {
            val user = getUserDataFromUi()
            userDao.delete(user)
        }
    }

    fun deleteButtonClicked() {
        uiScope.launch {
            deleteUser()
            _deleteButtonClicked.value = true
        }
    }

    fun doneDeletingUser() {
        _deleteButtonClicked.value = false
    }

    fun updateButtonClicked() {
        uiScope.launch {
            updateUser()
            _updateButtonClicked.value = true
        }
    }

    fun doneUpdatingUser() {
        _updateButtonClicked.value = false
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}