package com.example.kotlinfundamentals.userdetail

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.kotlinfundamentals.R
import com.example.kotlinfundamentals.database.UserDatabase
import com.example.kotlinfundamentals.databinding.UserDetailFragmentBinding
import com.example.kotlinfundamentals.login.LoginFragmentDirections
import com.example.kotlinfundamentals.login.LoginViewModelFactory
import com.google.android.material.snackbar.Snackbar

class UserDetailFragment : Fragment() {

    private lateinit var viewModel: UserDetailViewModel
    private lateinit var binding : UserDetailFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<UserDetailFragmentBinding>(inflater,
            R.layout.user_detail_fragment,container,false)

        (activity as AppCompatActivity).supportActionBar?.title = "User Details"

        val application = requireNotNull(this.activity).application

        val dataSource = UserDatabase.getInstance(application).userDao
        val viewModelFactory = UserDetailViewModelFactory(dataSource, application)

        viewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(UserDetailViewModel::class.java)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        val argsId = UserDetailFragmentArgs.fromBundle(requireArguments()).clickedUserId
        binding.txtUserId.setText(argsId.toString())
        viewModel.onUserClicked(argsId)

        //Toast.makeText(context, viewModel.imageUrl.value, Toast.LENGTH_SHORT).show()

        Glide.with(this).
        load("https://source.unsplash.com/1600x600")
            .into(binding.imgUser)

        observeUI()

        return binding.root
    }

    private fun observeUI () {
        viewModel.userId.observe(viewLifecycleOwner, Observer {

            viewModel.onUserClickedDone()
        })

        viewModel.emailFieldContent.observe(viewLifecycleOwner,
            Observer {
                })

        viewModel.passwordFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.nameFieldContent.observe(viewLifecycleOwner,
            Observer {

            })

        viewModel.deleteButtonClicked.observe(viewLifecycleOwner, Observer { deleteUser ->
            if (deleteUser) {
                var snack = Snackbar.make(requireActivity().findViewById(android.R.id.content),
                    "User Deleted!", Snackbar.LENGTH_SHORT)
                snack.show()
                findNavController().navigate(UserDetailFragmentDirections.actionUserDetailFragmentToHomeFragment())
                viewModel.doneDeletingUser()
            }
        })

        viewModel.updateButtonClicked.observe(viewLifecycleOwner, Observer { updateUser ->
            if (updateUser) {
                var snack = Snackbar.make(requireActivity().findViewById(android.R.id.content),
                    "User Updated!", Snackbar.LENGTH_SHORT)
                snack.show()
                findNavController().navigate(UserDetailFragmentDirections.actionUserDetailFragmentToHomeFragment())
                viewModel.doneUpdatingUser()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }
}