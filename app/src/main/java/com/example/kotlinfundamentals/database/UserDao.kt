package com.example.kotlinfundamentals.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UserDao {

    @Insert
    fun insert(user: User)

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)

    @Query("SELECT * from users_table WHERE userId = :key")
    fun getUserById(key: Long): User?

    @Query("SELECT * FROM users_table")
    fun getAllUsers(): LiveData<List<User>>

    @Query("SELECT userId FROM users_table WHERE email = :email AND password = :password")
    fun authorizeUser(email: String, password: String): Long

    @Query("DELETE FROM users_table")
    fun clearTable()
}