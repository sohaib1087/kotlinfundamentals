package com.example.kotlinfundamentals.property

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.kotlinfundamentals.R
import com.example.kotlinfundamentals.databinding.PropertyFragmentBinding

class PropertyFragment: Fragment() {
    private lateinit var viewModel: PropertyViewModel
    private lateinit var binding : PropertyFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<PropertyFragmentBinding>(inflater,
            R.layout.property_fragment, container,false)

        (activity as AppCompatActivity).supportActionBar?.title = "Mars Properties"

        val viewModelFactory = PropertyViewModelFactory()

        viewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(PropertyViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.getMarsRealEstateProperties()

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }
}