package com.example.kotlinfundamentals.property

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.kotlinfundamentals.home.HomeViewModel

class PropertyViewModelFactory : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PropertyViewModel::class.java)) {
            return PropertyViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}